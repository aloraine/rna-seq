#!/usr/bin/env python2.7

"""Make annots.xml for RNA-Seq Quickload site."""

# https://bitbucket.org/lorainelab/igbquickload/src/master/
from Quickload import *
from AnnotsXmlForRNASeq import *
import QuickloadUtils as utils

def readCodeBook(fname="../Count/data/codebook.txt"):
    lsts = []
    fh=open(fname,'r')
    while 1:
        line = fh.readline()
        if not line:
            break
        toks=line.rstrip().split(',')
        lsts.append(toks)
    return lsts

def main():
    genome_version="S_lycopersicum_Sep_2019"
    physical_folder="PGRP1939255"
    url=genome_version+"/"+physical_folder
    user_friendly_folder_name="Pollen Tube PGRP1939255"
    lsts = readCodeBook()
    newlsts=[]
    for lst in lsts[1:]:
        sample=lst[0]
        filename_prefix=re.sub(".bam","",lst[2])
        color=lst[6][1:]
        description=lst[7]
        newlsts.append([filename_prefix,description,color,"ffffff",url])
        samples=makeQuickloadFilesForRNASeq(lsts=newlsts,folder=user_friendly_folder_name,
                                            deploy_dir=physical_folder,include_FJ=True,
                                            include_scaled_bw_by_strand=True)
    txt = makeAnnotsXml(quickload_files=samples)
    fname="annots.xml"
    fh=open(fname,'w')
    fh.write(txt)
    fh.close()

if __name__ == '__main__':
    about="Make annots.xml text."
    utils.checkForHelp(about)
    main()
