#!/usr/bin/env python

ex=\
"""
Read GFF from stdin.
Ignore lines that lack Cufflinks in field two.
Change field score field to "0" instead of "Cufflinks".
Write to stdout.
"""

import sys,fileinput,optparse

tab='\t'
nl='\n'

def main(args):
    for line in fileinput.input(args):
        # ignore commented lines
        if line.startswith('#'):
            continue
        toks = line.strip().split(tab)
        if toks[1]=="Cufflinks":
            toks[5]="0"
            sys.stdout.write(tab.join(toks)+nl)

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)

