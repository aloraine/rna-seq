---
title: "Heat stress changes expression of heat-related genes in tomato pollen tubes cultivated *in vitro*"
author: "Ben Styler and Sorel Ouonkap Yimga. Modified from Ann Loraine"
date: "6/16/2020"
output: html_document
---

* * *

## Set-up
# Introduction

Load custom functions:

```{r message=FALSE, warning=FALSE}
source("../src/Common.R")
```

This Markdown analyzes RNA-Seq gene expression data from in vitro cultivated pollen tubes exposed to heat stress.

Pollen tubes from `r length(getVarieties())` tomato varieties were tested, including:


```{r}
source("../src/Common.R")
getVarieties()
```

The experiment included `r length(getSampleNames())` samples, up to `r length(unique(getReplicate(getSampleNames())))` replicates per variety. 

Questions:

* How many genes were differentially expressed?

Experimental protocol:

* Pollen samples were incubated in germination fluid to trigger development of pollen tubes. There were three samples per variety.
* Following germination, an aliquot from each sample was moved to a new tube and exposed to heat stress. 
* The remaining amount per tube was incubated at the non-heat-stress temperature following addition of fresh germination media.
* After further 3-hours incubation, control and treatment samples were harvested and used for library preparation.
* Libraries were sequenced and the number of overlapping reads per gene were counted. (See [Counts module](https://bitbucket.org/hotpollen/rna-seq/src/master/Count/).)

The replicates were paired in that control (C) and treatment (S) samples originated from the same sample tube. MDS plots (see [Counts module](https://bitbucket.org/hotpollen/rna-seq/src/master/Count/)) showed a weak replicate effect.

Therefore this analysis considers replicate number as a factor to account for the paired nature of the samples.

* * *

Load required library from the Bioconductor project:

```{r message=FALSE, warning=FALSE, paged.print=FALSE}
library(edgeR)
```

Load required data:

* gene information
* sample colors
* counts
* scaled counts
* sample information (code book) data frames:

```{r message=FALSE, warning=FALSE, paged.print=FALSE}
source("../src/Common.R")
annotations=getGeneDescription()
sample_colors=getSampleColors()
counts=getCounts()
fpm=cpm(counts)
sample_info=getCodeBook()
```

Define a function that uses a paired model to identify differentially expressed genes:

```{r }
getDeGenes = function(counts,sample_info,group1_name,group2_name) {
  group1_indexes=grep(group1_name,names(counts)) 
  group2_indexes=grep(group2_name,names(counts))
  indexes=c(group1_indexes,group2_indexes)
  sample_groups=sample_info[indexes,c("replicate","group","variety")]
  sample_groups=droplevels(sample_groups)
  little_DGEList=DGEList(counts[,indexes],
                         group = sample_groups$group,
                         remove.zeros = TRUE)
  design=model.matrix(~replicate+group,data=sample_groups)
  little_DGEList = estimateDisp(little_DGEList,design)
  fit = glmQLFit(little_DGEList, design)
  lrt=glmQLFTest(fit)
  results=lrt$table
  results$Q=p.adjust(results$PValue,method="fdr")
  results$gene=row.names(results)
  results$group1=group1_name
  results$group2=group2_name
  results$variety=droplevels(as.factor(sample_groups$variety[1]))
  return(results)
}
```

**Note**: The above procedure follows Section 3.4.1 titled "Paired samples" from the edgeR User's Guide, version last revised 21 October 2019.

Define a function to merge differential expression results with annotations data, keeping results with FDR of Q or better:

```{r}
processResults = function(results,annotations,fpm,Q=0.01) {
  to_keep=results$Q<Q
  results=results[to_keep,c("gene","variety","group1","group2","Q","logFC")]
  results=merge(results,annotations,by.x = "gene",by.y="gene")
  group1_name=unique(results$group1)
  group2_name=unique(results$group2)
  group1_ave=signif(rowMeans(fpm[,grep(group1_name,colnames(fpm))]),3)
  group2_ave=signif(rowMeans(fpm[,grep(group2_name,colnames(fpm))]),3)  
  aves=data.frame(gene=row.names(fpm),
                  group1.ave=group1_ave,
                  group2.ave=group2_ave)
  results=merge(results,aves,by.x="gene",by.y="gene")
  o=order(results$logFC,decreasing = TRUE)
  results = results[o,c("gene","variety","group1","group1.ave",
                        "group2","group2.ave","logFC","Q",
                        "description")]
  results$logFC=round(results$logFC,3)
  results$Q=signif(results$Q,3)
  return(results)
}
```

## Comparisons

Create data frame with comparisons:

```{r}
SetComparisons = function(group1, group2)
{
comparisons=data.frame(group1, group2)
row.names(comparisons)=paste0(comparisons$group1,"to",comparisons$group2)
Q=0.01
lsts=apply(comparisons,1,function(x)getDeGenes(counts,sample_info,x[1],x[2]))
results=lapply(lsts,function(x)processResults(x,annotations,fpm,0.01))
df=do.call("rbind", results)
rownames = paste0(df$gene,"-",df$group1,"to",df$group2)
row.names(df)=rownames
return(df)
}
```


